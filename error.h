#ifndef ERROR_H
#define ERROR_H

/* Códigos de retorno */
enum {
    SUCCESS  = 0, /* operação realizada com sucesso.              */
    PARAM_E  = 1, /* erro de parâmetro inválido.                  */
    MEMORY_E = 2, /* erro de alocação de memória.                 */
    BUFFER_E = 3, /* memória insuficiente para executar operação. */
};

#ifdef DEBUG
  #include <stdio.h>
  #define debug printf
#else
  #define debug(...) ((void)0)
#endif

#endif